#!/usr/bin/env groovy

def call(String imageName){
    echo 'building the docker image of the application '
    withCredentials([usernamePassword(credentialsId: 'docker-hub-account', passwordVariable: 'PASS',
     usernameVariable: 'USER')]) {
                      sh "docker build -t $USER/$imageName ."
                      sh 'echo $PASS | docker login -u $USER --password-stdin'
                      sh "docker push $USER/$imageName"
                      }
}

